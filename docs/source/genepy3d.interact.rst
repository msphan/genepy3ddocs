genepy3d.interact package
=========================

Submodules
----------

genepy3d.interact.curvesurface module
-------------------------------------

.. automodule:: genepy3d.interact.curvesurface
   :members:
   :undoc-members:
   :show-inheritance:

genepy3d.interact.pointsurface module
-------------------------------------

.. automodule:: genepy3d.interact.pointsurface
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: genepy3d.interact
   :members:
   :undoc-members:
   :show-inheritance:
