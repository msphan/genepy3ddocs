genepy3d.io package
===================

Submodules
----------

genepy3d.io.base module
-----------------------

.. automodule:: genepy3d.io.base
   :members:
   :undoc-members:
   :show-inheritance:

genepy3d.io.catmaid module
--------------------------

.. automodule:: genepy3d.io.catmaid
   :members:
   :undoc-members:
   :show-inheritance:

genepy3d.io.swc module
----------------------

.. automodule:: genepy3d.io.swc
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: genepy3d.io
   :members:
   :undoc-members:
   :show-inheritance:
