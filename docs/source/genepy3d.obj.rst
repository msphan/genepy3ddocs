genepy3d.obj package
====================

Submodules
----------

genepy3d.obj.curves module
--------------------------

.. automodule:: genepy3d.obj.curves
   :members:
   :undoc-members:
   :show-inheritance:

genepy3d.obj.points module
--------------------------

.. automodule:: genepy3d.obj.points
   :members:
   :undoc-members:
   :show-inheritance:

genepy3d.obj.surfaces module
----------------------------

.. automodule:: genepy3d.obj.surfaces
   :members:
   :undoc-members:
   :show-inheritance:

genepy3d.obj.trees module
-------------------------

.. automodule:: genepy3d.obj.trees
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: genepy3d.obj
   :members:
   :undoc-members:
   :show-inheritance:
