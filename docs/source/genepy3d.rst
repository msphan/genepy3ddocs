genepy3d package
================

Subpackages
-----------

.. toctree::

   genepy3d.interact
   genepy3d.io
   genepy3d.obj
   genepy3d.util

Module contents
---------------

.. automodule:: genepy3d
   :members:
   :undoc-members:
   :show-inheritance:
